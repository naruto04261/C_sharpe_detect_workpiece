﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenCvSharp;
using Point = OpenCvSharp.Point;
using Size = OpenCvSharp.Size;

namespace opencvtest
{
    public partial class Form1 : Form
    {
        class Cirlcle
        {
            public double centerX, centerY;
            public double radius;
            public String text;
            public Point text_pos;
            public int line1_x1;
            public int line1_x2;
            public int line1_y;
            public int line2_y1;
            public int line2_y2;
            public int line2_x;
            public double arrow_x2;
            public double arrow_y2;
        }
        class SaveConvertLinePoints
        {
            public Point point1, point2;
        }
        class LineSave
        {
            public List <LineSegmentPoint> lines = new List <LineSegmentPoint>();
            public double posy,posx,angle;
        }
        class detectSave
        {
            public Mat pre_image=new Mat();
            public List<LineDrawSave> lineDrawSave = new List<LineDrawSave>();
        }
        class LineDrawSave
        {
            public Point p1 = new Point();
            public Point p2 = new Point();
            public double angle = new double();
            public double dis = new double();
        }
        class Save_var
        {
            public Mat img_copy1;
            public List<Cirlcle> cirlcles;
            public int count;
        }
        public Boolean box3_lock = true, box3_mousedown = false, box3_mouseup = false;
        public int box3_mode = 0, box3_x1 = 0, box3_x2 = 0, box3_y1 = 0, box3_y2 = 0;
        public double box3_zoom_value = 1;
        public Mat box3_image = new Mat();
        public Mat image_cp = new Mat();
        public Mat src_backup = new Mat();
        private List<Cirlcle> box3_circles = new List<Cirlcle>();
        Int64 bit_width;
        Int64 bit_height;
        int b_w, b_h;
        int x_start, y_start;
        float[,] myLengthsArray = new float[3, 3] { { -1, -1, -1 }, { -1, 9, -1 }, { -1, -1, -1 } };
        Mat kernel;// Scalar.;//= (Mat<float>(3, 3) << 0, -1, 0, 0, 5, 0, 0, -1, 0);
        List<RotatedRect> ellipse_save;
        public static Bitmap MatToBitmap(Mat image)
        {
            return OpenCvSharp.Extensions.BitmapConverter.ToBitmap(image);
        }
        public static Mat BitmapToMap(Bitmap image)
        {
            return OpenCvSharp.Extensions.BitmapConverter.ToMat(image);
        }
        public Form1()
        {
            InitializeComponent();
            kernel = new Mat(3, 3, MatType.CV_32F, myLengthsArray);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                box3_lock = true;
                ellipse_save = new List<RotatedRect>();
                src_backup = new Mat(openFileDialog1.FileName, ImreadModes.Grayscale);
                Mat src4 = new Mat(openFileDialog1.FileName, ImreadModes.Grayscale);
                Mat src3 = new Mat(openFileDialog1.FileName, ImreadModes.Color);
                Mat src2 = new Mat(openFileDialog1.FileName, ImreadModes.Grayscale);
                Mat src = new Mat(openFileDialog1.FileName, ImreadModes.Grayscale);
                Mat src1 = new Mat(openFileDialog1.FileName, ImreadModes.Grayscale);
                
                Cv2.Filter2D(src1, src2, MatType.CV_8UC3, kernel);
                Cv2.Erode(src2, src2, kernel);
                Cv2.Dilate(src2, src2, kernel);
                Cv2.Canny(src2, src2, 150, 200);
                
                //Cv2.Canny(src, dst, 50, 200);
                OpenCvSharp.Size size;
                size = new OpenCvSharp.Size(17, 17);
                OpenCvSharp.Size ksize = new OpenCvSharp.Size(4, 4);
                Mat dst2 = new Mat();
                Cv2.Erode(src1, dst2, kernel);
                Cv2.Dilate(src1, dst2, kernel);
                Cv2.Canny(dst2, dst2, 100, 100);

                Cv2.Erode(src, src, kernel);
                Cv2.Dilate(src, src, kernel);
                Cv2.Canny(src, src, 50, 150);
                src = src - src.MedianBlur(5);
                Rect sizeofsrc2 = new Rect(new OpenCvSharp.Point(0, 0), src2.Size());
                Mat mask = new Mat(src2, sizeofsrc2);
                src3.CopyTo(src2, mask);
                bit_width = src2.Width;
                bit_height = src2.Height;
                Mat src2_cover = src2.MedianBlur(3);
                Cv2.Filter2D(src4, src4, MatType.CV_8UC3, kernel);
                var mat10 = new Mat<Vec3b>(src2_cover); // cv::Mat_<cv::Vec3b>
                var indexer1 = mat10.GetIndexer();
                Int64 minx = bit_width, maxx = 0, miny = bit_height, maxy = 0;
                List<Vec3b> all_color = new List<Vec3b>();

                for (int y = 0; y < bit_height; y++)
                {
                    for (int x = 0; x < bit_width; x++)
                    {
                        Vec3b color_now1 = indexer1[y, x];
                        if (color_now1.Item0>10 && color_now1.Item1 > 10 && color_now1.Item2 > 10)
                        {
                            all_color.Add(indexer1[y, x]);
                        }
                    }
                }
                var color_most = all_color.GroupBy(x => x)
                        .OrderByDescending(group => group.Count())
                            .First()
                                .Key;
                for (int y = 0; y < bit_height; y++)
                {
                    for (int x = 0; x < bit_width; x++)
                    {
                        Vec3b vec3B = indexer1[y, x];
                        double color1 = vec3B.Item0;
                        double color2 = vec3B.Item1;
                        double color3 = vec3B.Item2;
                        double m1 = color_most.Item0;
                        double m2 = color_most.Item1;
                        double m3 = color_most.Item2;
                        Boolean cp1 = color1 / m1 < 1.1 & color1 / m1 > 0.9;
                        Boolean cp2 = color2 / m2 < 1.1 & color2 / m2 > 0.9;
                        Boolean cp3 = color3 / m3 < 1.1 & color3 / m3 > 0.9;
                        if ( cp1 && cp2 && cp3)
                        {
                            if (x > maxx)
                            {
                                maxx = x;
                            }
                            if (x < minx)
                            {
                                minx = x;
                            }
                            if (y > maxy)
                            {
                                maxy = y;
                            }
                            if (y < miny)
                            {
                                miny = y;
                            }
                        }
                    }
                }
                x_start = (int)minx;
                y_start = (int)miny;
                Mat image = new Mat(src4.Size(), MatType.CV_8UC1, Scalar.White);
                
                Cv2.Rectangle(image, new OpenCvSharp.Point(minx, miny),
                    new OpenCvSharp.Point(maxx, maxy), Scalar.Black);
                b_w = (int)(maxx - minx);
                b_h = (int)(maxy - miny);
                image_cp = new Mat(src2, new Rect((int)minx, (int)miny, (int)(maxx - minx), (int)(maxy - miny)));
                for (int i = 0; i < 9; i++)
                {
                    image_cp = image_cp.Dilate(kernel);
                    image_cp = image_cp.Erode(kernel);
                }
                Cv2.CvtColor(image_cp, image_cp, ColorConversionCodes.BGR2GRAY);
                image_cp.ConvertTo(image_cp, MatType.CV_8UC1);
                image.CopyTo(box3_image);
                pictureBox1.Image = MatToBitmap(src_backup);
                pictureBox2.Image = MatToBitmap(src2_cover);
                pictureBox3.Image = MatToBitmap(box3_image);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int height = (int)bit_height, width = (int)bit_width;
            Mat img_copy1 = new Mat(new OpenCvSharp.Size(width, height), MatType.CV_8UC3, Scalar.Black);
            OpenCvSharp.Point[][] contours;
            HierarchyIndex[] hierachy; 
            int x = x_start, y = y_start;
            image_cp = image_cp.Dilate(kernel);
            image_cp = image_cp.Erode(kernel);
            
            Cv2.Threshold(image_cp, image_cp, 50, 255, ThresholdTypes.Binary);
            Cv2.GaussianBlur(image_cp, image_cp, new OpenCvSharp.Size((b_w / 120) * 2 + 1, (b_h / 120) * 2 + 1), 25);
            //Cv2.Canny(image_cp, image_cp, 0, 255);
            //Cv2.Filter2D(image_cp, image_cp, MatType.CV_8UC3, kernel);
            Cv2.FindContours(image_cp, out contours, out hierachy, RetrievalModes.Tree, ContourApproximationModes.ApproxTC89KCOS);
            int num = int.Parse(textBox1.Text);
            int count = 0;
            List<Cirlcle> cirlcles = new List<Cirlcle>();
            List<LineDrawSave> lineDrawSave=new List<LineDrawSave>();
            double distanse = Double.Parse(textBox3.Text) * 1000,
                   focus = Double.Parse(textBox2.Text);
            int xmin=(int)bit_width, ymin=(int)bit_height, xmax=0, ymax=0;
            Boolean rect_flag = false;
            for (int i = 0; i < contours.Length; i++)
            {
                //拟合函数必须至少5个点，少于则不拟合
                if (contours[i].Length < (bit_width+bit_height)/30) continue;
                //椭圆拟合

                var rrt = Cv2.FitEllipse(contours[i]);

                //ROI复原
                rrt.Center.X += x;
                rrt.Center.Y += y;
                float w = rrt.Size.Width;
                float h = rrt.Size.Height;
                Rect rectangle = Cv2.BoundingRect(contours[i]);
                if (w / h > 1.3 || w / h < 0.7 || (float)w / b_w > 1.01 || (float)h / b_h > 1.01)
                {
                    rect_flag = true;
                    if (xmin> x + rectangle.X)
                    {
                        xmin = x + rectangle.X;
                    }
                    if (ymin > y + rectangle.Y)
                    {
                        ymin = y + rectangle.Y;
                    }
                    if (xmax < x + rectangle.X + rectangle.Width)
                    {
                        xmax = x + rectangle.X + rectangle.Width;
                    }
                    if (ymax < y + rectangle.Y + rectangle.Height)
                    {
                        ymax = y + rectangle.Y + rectangle.Height;
                    }
                    //Cv2.Rectangle(img_copy1, new OpenCvSharp.Point(x + rectangle.X, y + rectangle.Y),
                    //               new OpenCvSharp.Point(x + rectangle.X + rectangle.Width, y + rectangle.Y + rectangle.Height), new Scalar(255, 255, 255), 1);
                    //Cv2.Rectangle(image_cp, new OpenCvSharp.Point(rectangle.X, rectangle.Y),
                    //               new OpenCvSharp.Point(rectangle.X + rectangle.Width, rectangle.Y + rectangle.Height), Scalar.Chocolate, 5);
                    continue;
                }
                //檢測圓形
                double r2 = (w + h) / 4;
                Save_var save_var = detect_circle(img_copy1, cirlcles, count, rrt, r2, distanse, focus);
                cirlcles = save_var.cirlcles;
                img_copy1 = save_var.img_copy1;
                count = save_var.count;
            }
            
            if (rect_flag)
            {
                int x_catch_min=(int)bit_width,y_catch_min=(int)bit_height;
                if ((float)xmin / bit_width < 0.05)
                {
                    xmin = 0;
                }
                else
                {
                    xmin = xmin - 1;
                }
                if ((float)(xmax) / bit_width > 0.95)
                {
                    xmax = (int)bit_width;
                }
                else if (xmax!= bit_width)
                {
                    xmax = xmax + 1;
                }
                if ((float)(ymin) / bit_width < 0.05)
                {
                    ymin = 0;
                }
                else if (ymin != 0)
                {
                    ymin = ymin - 1;
                }
                if (ymax != bit_height)
                {
                    ymax = ymax - 1;
                }
                else if ((float)(ymax) /bit_height > 0.95)
                {
                    ymax = (int)bit_height;
                }
                Mat pre_image_back = new Mat(src_backup, new Rect(xmin, ymin, xmax - xmin, ymax - ymin));
                Mat pre_image = new Mat(src_backup, new Rect(xmin, ymin, xmax - xmin, ymax - ymin));
                pre_image=pre_image.MedianBlur(11);
                //pre_image=pre_image.Filter2D(MatType.CV_8U, kernel);
                pre_image=pre_image.Canny(0, 255);
                foreach(var elps in ellipse_save)
                {
                    RotatedRect elps1 = elps;
                    elps1.Center.X = elps1.Center.X + xmin;
                    elps1.Center.Y = elps1.Center.Y + ymin;
                    if (elps1.Size.Height > elps1.Size.Width)
                    {
                        elps1.Size.Width = elps1.Size.Height;
                    }
                    else
                    {
                        elps1.Size.Height = elps1.Size.Width;
                    }
                    pre_image.Rectangle(elps1.BoundingRect(), Scalar.Black,-1);
                }
                //pictureBox1.Image = MatToBitmap(pre_image);
                double thresh = ((double)(xmax - xmin + ymax - ymin) / 400);
                if (thresh < 5)
                {
                    thresh = 5;
                }
                List<Point> line_points = new List<Point>();
                Cv2.FindContours(pre_image, out contours, out hierachy, RetrievalModes.Tree, ContourApproximationModes.ApproxTC89KCOS);
                Mat pre_cp = new Mat(new OpenCvSharp.Size(pre_image.Width,pre_image.Height), MatType.CV_8U, Scalar.Black);
                List <LineSave> lineSave=new List <LineSave>();
                for (int i = 0; i < contours.Length; i++)
                {
                    if (contours[i].Length < 4) continue;
                    Rect rectangle = Cv2.BoundingRect(contours[i]);
                    Mat child_rect = new Mat(pre_image, rectangle);
                    RotatedRect rect_rot = Cv2.MinAreaRect(contours[i]);
                    lineSave = detect_line(lineSave, child_rect, thresh, line_points,
                        rectangle, rect_rot,false);
                }
                detectSave detect_save = detect_line_to_draw(lineSave, pre_image, lineDrawSave);
                pre_image = detect_save.pre_image;
                lineDrawSave = detect_save.lineDrawSave;
                Cv2.FindContours(pre_image, out contours, out hierachy, RetrievalModes.Tree, ContourApproximationModes.ApproxTC89KCOS);
                
                for (int i = 0; i < contours.Length; i++)
                {
                    if (contours[i].Length < 5) continue;
                    //Rect rectangle = Cv2.BoundingRect(contours[i]);
                    Mat cont = new Mat(new Size(pre_image.Width, pre_image.Height), MatType.CV_8U, Scalar.Black) ;
                    List<List<Point>> contour_part = new List<List<Point>>();
                    contour_part.Add(contours[i].ToList());
                    Cv2.DrawContours(cont, contour_part, -1, Scalar.White);
                    RotatedRect ellipese = Cv2.FitEllipse(contours[i]);
                    double e_for_rad=0;
                    if (ellipese.Size.Width < ellipese.Size.Height)
                    {
                        e_for_rad = ellipese.Size.Height / ellipese.Size.Width;
                    }
                    else
                    {
                        e_for_rad = ellipese.Size.Width / ellipese.Size.Height;
                    }
                    if (e_for_rad > 3)
                    {
                        
                    }
                    else
                    {
                        if (ellipese.Size.Width< (ymax - ymin + xmax-xmin) / 20 
                            && ellipese.Size.Height < (ymax - ymin + xmax - xmin) / 20)
                        {
                            continue;
                        }
                        if (ellipese.Center.X < 0 || ellipese.Center.Y < 0) continue;
                        //ellipese.Center.X += xmin;
                        //ellipese.Center.Y += ymin;
                        Point[][] cont_circ;
                        HierarchyIndex[] hier_circ;
                        List<Point> cont_list = contours[i].ToList();
                        int x_min_circ = cont_list.OrderByDescending(gp => gp.X).Last().X,
                            x_max_circ = cont_list.OrderByDescending(gp => gp.X).First().X,
                            y_min_circ = cont_list.OrderByDescending(gp => gp.Y).Last().Y,
                            y_max_circ = cont_list.OrderByDescending(gp => gp.Y).First().Y;
                        Mat test2 = new Mat(pre_image.Size(), MatType.CV_8U, Scalar.Black);
                        var list_pts = new List<List<Point>>();
                        list_pts.Add(cont_list);
                        test2.DrawContours(list_pts, -1, Scalar.White);
                        Mat test3 = new Mat(test2, new Rect(x_min_circ, y_min_circ, x_max_circ - x_min_circ, y_max_circ - y_min_circ));
                        test3.FindContours(out cont_circ, out hier_circ, RetrievalModes.Tree, ContourApproximationModes.ApproxNone);
                        Line2D fitline = Cv2.FitLine(cont_circ[0], DistanceTypes.Fair, 0, 0, 0.1);
                        double m = fitline.Vy / fitline.Vx;
                        double b = fitline.Y1 - m * fitline.X1;
                        double b_elps = ellipese.Size.Width / 2, a_elps = ellipese.Size.Height / 2;
                        double c_elps = Math.Sqrt(a_elps * a_elps - b_elps * b_elps);
                        double epsilon = Math.Atan2(b_elps, c_elps);
                        double sin_eps = Math.Sin(epsilon);
                        //double rad_elps = (b_elps) * (b_elps) / (a_elps) / Math.Sqrt(sin_eps * sin_eps * sin_eps);
                        double rad_elps = Math.Sqrt(a_elps*a_elps+b_elps*b_elps);
                        int x_dash = 1, y_dash = 1;
                        double center_to_line = m * (ellipese.Center.X) + b - ellipese.Center.Y;
                        if (m < 0)
                        {
                            if (center_to_line > 0)
                            {
                                x_dash = -1;
                                y_dash = -1;
                            }
                            else
                            {
                                x_dash = 1;
                                y_dash = 1;
                            }
                        }
                        else
                        {
                            if (center_to_line > 0)
                            {
                                x_dash = -1;
                                y_dash = 1;
                            }
                            else
                            {
                                x_dash = 1;
                                y_dash = -1;
                            }
                        }
                        double x_nd_mv = x_dash * Math.Abs((rad_elps - b_elps) * Math.Sin(ellipese.Angle / 180 * Math.PI)),
                            y_nd_mv = y_dash * Math.Abs((rad_elps - b_elps) * Math.Cos(ellipese.Angle / 180 * Math.PI));
                        img_copy1.Circle((Point)ellipese.Center + new Point(x_nd_mv+xmin, y_nd_mv+ymin), (int)Math.Round(rad_elps), Scalar.White, 1);
                        
                        Cirlcle cirlcle = new Cirlcle();
                        cirlcle.centerX = ((Point)ellipese.Center + new Point(x_nd_mv + xmin, y_nd_mv + ymin)).X;
                        cirlcle.centerY = ((Point)ellipese.Center + new Point(x_nd_mv + xmin, y_nd_mv + ymin)).Y;
                        cirlcle.radius = (int)Math.Round(rad_elps);
                        cirlcles.Add(cirlcle);
                        double end_x = cirlcle.centerX + Math.Round(rad_elps) * Math.Cos(30 * 1.0 / 180 * Math.PI);
                        double end_y = cirlcle.centerY + Math.Round(rad_elps) * Math.Sin(30 * 1.0 / 180 * Math.PI);
                        OpenCvSharp.Point text_po;
                        double move = 20;
                        if (move > Math.Round(rad_elps) / 2)
                        {
                            move = Math.Round(rad_elps) / 2;
                        }
                        text_po.X = (int)((end_x + cirlcle.centerX - move) / 2);
                        text_po.Y = (int)((end_y + cirlcle.centerY - move) / 2);

                        cirlcle.arrow_x2 = end_x;
                        cirlcle.arrow_y2 = end_y;
                        cirlcle.text_pos = text_po;
                        cirlcle.text = (distanse / focus * Math.Round(rad_elps)).ToString("#0.0");
                        cirlcle.line1_x1 = (int)(cirlcle.centerX) - img_copy1.Width / 40;
                        cirlcle.line1_x2 = (int)(cirlcle.centerX) + img_copy1.Width / 40;
                        cirlcle.line1_y = (int)(cirlcle.centerY);
                        cirlcle.line2_y1 = (int)(cirlcle.centerY) - img_copy1.Width / 40;
                        cirlcle.line2_y2 = (int)(cirlcle.centerY) + img_copy1.Width / 40;
                        cirlcle.line2_x = (int)(cirlcle.centerX);
                        cirlcles.Add(cirlcle);
                        //pre_image.Circle((Point)ellipese.Center + new Point(x_nd_mv, y_nd_mv), (int)Math.Round(rad_elps), Scalar.White, 1);
                        ellipese.Center.X += xmin;
                        ellipese.Center.Y += ymin;
                        Cv2.Ellipse(img_copy1, ellipese, Scalar.White,1);
                        /*
                        SaveConvertLinePoints point12 = Convert_points_to_line_points(cont_circ[0],
                            0, x_max_circ - x_min_circ, 0, y_max_circ - y_min_circ,
                            x_min_circ, y_min_circ);
                        Cv2.Line(pre_image, point12.point1, point12.point2, Scalar.White, 2);
                        */
                        Cv2.Rectangle(pre_image, ellipese.BoundingRect(), Scalar.Black,-1);
                    }
                }
                //畫剩餘的輪廓
                
                thresh = ((double)(xmax - xmin + ymax - ymin) / 400);
                if (thresh < 5)
                {
                    thresh = 5;
                }
                Rect rectangle1 = new Rect(0,0,0,0);
                Mat child_rect1 = new Mat();
                pre_image.CopyTo(child_rect1);
                RotatedRect rect_rot1 = new RotatedRect();
                line_points = new List<Point>();
                lineSave = new List<LineSave>();
                lineSave = detect_line(lineSave, child_rect1, thresh, line_points,
                    rectangle1, rect_rot1,true);
                detectSave detect_save1 = detect_line_to_draw(lineSave, pre_image, lineDrawSave);
                pre_image = detect_save1.pre_image;
                lineDrawSave = detect_save1.lineDrawSave;
                Boolean stop_draw_flag = true;
                double near_dis = 0;
                if (lineDrawSave.Count != 0)
                {
                    near_dis = lineDrawSave.OrderByDescending(gp => gp.dis).First().dis/30;
                }
                //合併線條
                var line_gps = lineDrawSave.GroupBy(gp => (int)Math.Round(gp.angle / 5) * 5).Where(gp=> gp.ToList().Count>1);
                int gp_c = line_gps.Count();
                for (int i=0;i<line_gps.Count();i++)
                {
                    var line_gp = line_gps.ElementAt(i);
                    var lines_in_gp = line_gp.ToList();
                    int line_gp_count = lines_in_gp.Count;
                    int remove=0;
                    for (int j = line_gp_count - 1 - remove; j >=0 ; j--)
                    {
                        for (int k = line_gp_count - 1 - remove; k >= j + 1 ; k--)
                        {
                            Point p1_1 = lines_in_gp[j].p1,
                                  p1_2 = lines_in_gp[j].p2,
                                  p2_1 = lines_in_gp[k].p1,
                                  p2_2 = lines_in_gp[k].p2;
                            int x1_org = p1_1.X;
                            int y1_org = p1_1.Y;
                            int x2_org = p1_2.X;
                            int y2_org = p1_2.Y;
                            int x1_comp = p2_1.X;
                            int y1_comp = p2_1.Y;
                            int x2_comp = p2_2.X;
                            int y2_comp = p2_2.Y;
                            double dis1 = Math.Sqrt((x1_org - x1_comp) * (x1_org - x1_comp) + (y1_org - y1_comp) * (y1_org - y1_comp)),
                                   dis2 = Math.Sqrt((x2_org - x2_comp) * (x2_org - x2_comp) + (y2_org - y2_comp) * (y2_org - y2_comp)),
                                   dis3 = Math.Sqrt((x1_org - x2_comp) * (x1_org - x2_comp) + (y1_org - y2_comp) * (y1_org - y2_comp)),
                                   dis4 = Math.Sqrt((x2_org - x1_comp) * (x2_org - x1_comp) + (y2_org - y1_comp) * (y2_org - y1_comp));
                            if (dis1 < near_dis || dis2 < near_dis || dis3 < near_dis || dis4 < near_dis)
                            {
                                var x_combine = new List<int>();
                                var y_combine = new List<int>();
                                x_combine.Add(lines_in_gp[j].p1.X);
                                x_combine.Add(lines_in_gp[j].p2.X);
                                x_combine.Add(lines_in_gp[k].p1.X);
                                x_combine.Add(lines_in_gp[k].p2.X);
                                y_combine.Add(lines_in_gp[j].p1.Y);
                                y_combine.Add(lines_in_gp[j].p2.Y);
                                y_combine.Add(lines_in_gp[k].p1.Y);
                                y_combine.Add(lines_in_gp[k].p2.Y);
                                int x_max_in_lines = x_combine.OrderByDescending(gp => gp).First();
                                int y_max_in_lines = y_combine.OrderByDescending(gp => gp).First();
                                int x_min_in_lines = x_combine.OrderByDescending(gp => gp).Last();
                                int y_min_in_lines = y_combine.OrderByDescending(gp => gp).Last();
                                List<Point> p1234 = new List<Point>();
                                p1234.Add(p1_1);
                                p1234.Add(p1_2);
                                p1234.Add(p2_1);
                                p1234.Add(p2_2);
                                SaveConvertLinePoints point12 = 
                                    Convert_points_to_line_points(p1234.ToArray(),
                                        x_min_in_lines,x_max_in_lines+1, 
                                        y_min_in_lines, y_max_in_lines+1,0, 0);
                                var line2_index = lineDrawSave.FindIndex(gp => (gp.p1 == p2_1 && gp.p2 == p2_2 && gp.angle == lines_in_gp[k].angle));
                                LineDrawSave new_line = new LineDrawSave();
                                new_line.p1 = point12.point1;
                                new_line.p2 = point12.point1;
                                Point p3 = new_line.p1 - new_line.p2;
                                new_line.angle = Math.Atan2(p3.X , p3.Y) / Math.PI * 180;
                                new_line.dis = Math.Sqrt(p3.X * p3.X + p3.Y * p3.Y);
                                if (new_line.dis < 0.1)
                                {
                                    if (y_max_in_lines - y_min_in_lines > x_max_in_lines - x_min_in_lines)
                                    {
                                        new_line.dis = y_max_in_lines - y_min_in_lines;
                                        new_line.angle = -90;
                                        new_line.p1 = new Point(new_line.p1.X, y_min_in_lines);
                                        new_line.p2 = new Point(new_line.p2.X, y_max_in_lines);
                                    }
                                    else
                                    {
                                        new_line.dis = x_max_in_lines - x_min_in_lines;
                                        new_line.angle = 0;
                                        new_line.p1 = new Point(x_min_in_lines,new_line.p1.Y);
                                        new_line.p2 = new Point(x_max_in_lines,new_line.p2.Y);
                                    }
                                    if (new_line.p1.X > x_max_in_lines)
                                    {
                                        new_line.p1.X = x_max_in_lines;
                                        new_line.p2.X = x_max_in_lines;
                                    }
                                    else if (new_line.p1.X < x_min_in_lines)
                                    {
                                        new_line.p1.X = x_min_in_lines;
                                        new_line.p2.X = x_min_in_lines;
                                    }
                                }
                                lineDrawSave[line2_index] = new_line;
                                lines_in_gp[k] = new_line;
                                var line1_index = lineDrawSave.FindIndex(gp => (gp.p1 == p1_1 && gp.p2 == p1_2 && gp.angle == lines_in_gp[j].angle));
                                lineDrawSave.RemoveAt(line1_index);
                                remove++;
                                lines_in_gp.RemoveAt(j);
                                if (j < 0)
                                {
                                    break;
                                }
                                //k--;
                            }
                        }
                    }
                }
                
                for (int i = 0; i < lineDrawSave.Count; i++)
                {
                    int y_dis = Math.Abs(lineDrawSave[i].p1.Y - lineDrawSave[i].p2.Y);
                    int x_dis = Math.Abs(lineDrawSave[i].p1.X - lineDrawSave[i].p2.X);
                    if (Math.Sqrt(x_dis* x_dis+ y_dis* y_dis) > (ymax - ymin) / 3)
                    {
                        stop_draw_flag = false;
                    }
                    
                }
                if (stop_draw_flag && lineDrawSave.Count!=0)
                {
                    img_copy1.Rectangle(new Rect(xmin, ymin, xmax - xmin, ymax - ymin),Scalar.White);
                    Cv2.PutText(img_copy1, (distanse / focus * (xmax - xmin)).ToString("#0.0"), new Point((xmax + xmin) / 2, ymax + 5), HersheyFonts.HersheyPlain, 2, Scalar.All(255));
                    Cv2.PutText(img_copy1, (distanse / focus * (ymax - ymin)).ToString("#0.0"), new Point(xmin+5, (ymax + ymin) / 2), HersheyFonts.HersheyPlain, 2, Scalar.All(255));
                }
                else
                {
                    if (lineDrawSave.Count != 0)
                    {
                        double max_dis = lineDrawSave.OrderByDescending(gp => gp.dis).First().dis;
                        for (int i = lineDrawSave.Count - 1; i >= 0; i--)
                        {
                            int x_comp = xmin, y_comp = ymin;
                            if (max_dis / 15 > lineDrawSave[i].dis)
                            {
                                lineDrawSave.RemoveAt(i);
                                continue;
                            }
                            Point test_pos = lineDrawSave[i].p1 + lineDrawSave[i].p2;
                            Point dis_pos = lineDrawSave[i].p1 - lineDrawSave[i].p2;
                            test_pos.X /= 2;
                            test_pos.Y /= 2;
                            if (Math.Abs(Math.Round(lineDrawSave[i].angle / 5) * 5) == 180 ||
                                Math.Round(lineDrawSave[i].angle / 5) * 5 == 0)
                            {
                                test_pos.X /= 2;
                            }
                            if (test_pos.Y / (double)bit_height < 0.05)
                            {
                                test_pos.Y += (int)((double)bit_height / 20);
                                if (max_dis / 10 > lineDrawSave[i].dis)
                                {
                                    lineDrawSave.RemoveAt(i);
                                    continue;
                                }
                            }
                            src_backup.Line(lineDrawSave[i].p1 + new Point(x_comp, y_comp), lineDrawSave[i].p2 + new Point(x_comp, y_comp), Scalar.White, 1, LineTypes.AntiAlias, 0);
                            img_copy1.Line(lineDrawSave[i].p1 + new Point(x_comp, y_comp), lineDrawSave[i].p2 + new Point(x_comp, y_comp), Scalar.White, 1, LineTypes.AntiAlias, 0);
                            Cv2.PutText(img_copy1, (distanse / focus * lineDrawSave[i].dis).ToString("#0.0") + "deg" + lineDrawSave[i].angle.ToString("#0.0"),
                                test_pos + new Point(x_comp, y_comp), HersheyFonts.HersheyPlain, 2, Scalar.All(255), 1, LineTypes.Link8, false);
                        }
                    }
                    
                }
                
                //pictureBox1.Image = MatToBitmap(pre_image);
            }

            foreach (Cirlcle circle1 in cirlcles)
            {
                Cv2.PutText(img_copy1, circle1.text, circle1.text_pos, HersheyFonts.HersheyPlain, 2, Scalar.All(255));
                Cv2.Circle(img_copy1, (int)(circle1.centerX), (int)(circle1.centerY), (int)circle1.radius, new Scalar(255, 255, 255), 1, LineTypes.Link8, 0);
                img_copy1.Line(circle1.line1_x1, circle1.line1_y, circle1.line1_x2, circle1.line1_y, Scalar.White);
                img_copy1.Line(circle1.line2_x, circle1.line2_y1, circle1.line2_x, circle1.line2_y2, Scalar.White);
                img_copy1 = DrawArrow(img_copy1, new OpenCvSharp.Point(circle1.centerX, circle1.centerY), new OpenCvSharp.Point(circle1.arrow_x2, circle1.arrow_y2), 15, 30, new Scalar(255, 255, 255));
                src_backup.Circle((int)(circle1.centerX), (int)(circle1.centerY), (int)circle1.radius, new Scalar(255, 255, 255), 1, LineTypes.Link8, 0);
            }
            box3_circles = cirlcles;
            img_copy1.CopyTo(box3_image);
            pictureBox1.Image = MatToBitmap(src_backup);
            pictureBox2.Image = MatToBitmap(image_cp);
            pictureBox3.Image = MatToBitmap(box3_image);
            box3_lock = false;
            box3_x1 = 0;
            box3_zoom_value = 1;
            box3_x2 = box3_image.Width;
            box3_y1 = 0;
            box3_y2 = box3_image.Height;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Mat test = new Mat(new Size(200, 200), MatType.CV_8UC1,Scalar.Black);
            RotatedRect elps = new RotatedRect(new Point2f(50, 50), new Size2f(20, 20), 0);
            test.Ellipse((Point)elps.Center,new Size(20, 20), elps.Angle, 180, 270, Scalar.White, 1);
            //test.Circle((Point)elps.Center, 40, Scalar.White, 1);
            Point[][] cont;
            HierarchyIndex[] hier;
            test.FindContours(out cont,out hier, RetrievalModes.Tree, ContourApproximationModes.ApproxNone);
            foreach(Point[] cont_ch in cont)
            {
                List<Point> cont_list=cont_ch.ToList();
                
                int xmin= cont_list.OrderByDescending(gp => gp.X).Last().X,
                    xmax= cont_list.OrderByDescending(gp => gp.X).First().X,
                    ymin = cont_list.OrderByDescending(gp => gp.Y).Last().Y,
                    ymax = cont_list.OrderByDescending(gp => gp.Y).First().Y;
                Mat test2 = new Mat(test.Size(), MatType.CV_8U, Scalar.Black);
                var list_pts = new List<List<Point>>();
                list_pts.Add(cont_list);
                test2.DrawContours(list_pts, -1, Scalar.White);
                Mat test3 = new Mat(test2, new Rect(xmin, ymin, xmax - xmin, ymax - ymin));
                test3.FindContours(out cont, out hier, RetrievalModes.Tree, ContourApproximationModes.ApproxNone);
                SaveConvertLinePoints point12 = Convert_points_to_line_points(cont[0],
                    0, xmax-xmin, 0, ymax-ymin,
                    xmin, ymin);
                Cv2.Line(test, point12.point1, point12.point2, Scalar.White, 2);
                Line2D fitline = Cv2.FitLine(cont[0], DistanceTypes.Fair, 0, 0, 0.1);
                double m = fitline.Vy / fitline.Vx;
                double b = fitline.Y1 - m * fitline.X1;
                RotatedRect rot = Cv2.FitEllipse(cont_ch);
                Rect eq_rot = rot.BoundingRect();
                double b_elps = rot.Size.Width/2, a_elps = rot.Size.Height/2;
                double c_elps = Math.Sqrt(a_elps*a_elps-b_elps*b_elps);
                double epsilon = Math.Atan2(b_elps, c_elps);
                double sin_eps = Math.Sin(epsilon);
                double rad_elps = (b_elps) * (b_elps) / (a_elps) /Math.Sqrt(sin_eps* sin_eps* sin_eps);
                int x_dash = 1, y_dash = 1;
                double center_to_line = m * (rot.Center.X) + b - rot.Center.Y;
                if (m < 0)
                {
                    if (center_to_line > 0)
                    {
                        x_dash = -1;
                        y_dash = -1;
                    }
                    else
                    {
                        x_dash = 1;
                        y_dash = 1;
                    }
                }
                else
                {
                    if (center_to_line > 0)
                    {
                        x_dash = 1;
                        y_dash = -1;
                    }
                    else
                    {
                        x_dash = -1;
                        y_dash = 1;
                    }
                }
                double x_nd_mv = x_dash * Math.Abs((rad_elps - b_elps) * Math.Sin(rot.Angle/180*Math.PI)),
                    y_nd_mv = y_dash * Math.Abs((rad_elps - b_elps) * Math.Cos(rot.Angle / 180 * Math.PI));
                test.Circle((Point)rot.Center+new Point(x_nd_mv,y_nd_mv), (int)Math.Round(rad_elps), Scalar.White, 1);
                //test.Ellipse(rot, Scalar.White, 1);
                
            }
            pictureBox2.Image = MatToBitmap(test);
        }
        private SaveConvertLinePoints Convert_points_to_line_points(Point[] line_points,double x1,double x2,
                                double y1,double y2,int move_x,int move_y)
        {
            Line2D fitline = Cv2.FitLine(line_points, DistanceTypes.Fair, 0, 0, 0.1);
            double m = fitline.Vy / fitline.Vx;
            double b = fitline.Y1 - m * fitline.X1;
            double lefty = x1 * m + b;
            double leftx = x1;
            double righty = (x2 - 1) * m + b;
            double rightx = x2 - 1;

            if (righty > y2 - 1)
            {
                if (m > 0.1)
                {
                    rightx += Math.Round(((y2 - 1 - righty) - b) / m);
                }
                righty = y2 - 1;
            }
            else if (righty < y1)
            {
                if (m > 0.1)
                {
                    rightx += Math.Round(((y1 - righty) - b) / m);
                }
                righty = y1;
            }
            else
            {
                righty = Math.Round(righty);
            }
            if (lefty > y2 - 1)
            {
                if (m > 0.1)
                {
                    leftx += Math.Round(((y2 - 1 - lefty) - b) / m);
                }
                lefty = y2 - 1;
            }
            else if (lefty < y1)
            {
                if (m > 0.1)
                {
                    leftx += Math.Round(((y1 - lefty) - b) / m);
                    if (leftx > x2 || leftx<x1)
                    {

                    }
                }
                lefty = y1;
            }
            else
            {
                lefty = Math.Round(lefty);
            }

            int line_start_x1 = move_x+(int)leftx,
                line_start_x2 = move_x+(int)rightx,
                line_start_y1 = move_y+(int)lefty,
                line_start_y2 = move_y+(int)righty;
            Point point1 = new Point(line_start_x1, line_start_y1);
            Point point2 = new Point(line_start_x2, line_start_y2);
            SaveConvertLinePoints saveConvertLinePoints = new SaveConvertLinePoints();
            saveConvertLinePoints.point1 = point1;
            saveConvertLinePoints.point2 = point2;
            return saveConvertLinePoints;
        }

        private void pictureBox3_MouseDown(object sender, MouseEventArgs e)
        {
            box3_mousedown = true;
            if (e.Button==MouseButtons.Left)
            {
                if (box3_mode == 1)
                {
                    box3_zoom_value += 0.2;
                    box3_x1 += (int)((box3_zoom_value - 1) / 2 * (double)box3_image.Width);
                    box3_x2 -= (int)((box3_zoom_value - 1) / 2 * (double)box3_image.Width);
                    box3_y1 += (int)((box3_zoom_value - 1) / 2 * (double)box3_image.Height);
                    box3_y2 -= (int)((box3_zoom_value - 1) / 2 * (double)box3_image.Height);
                    if (box3_y1 > box3_y2 || box3_x1 > box3_x2)
                    {
                        box3_zoom_value = 1;
                        box3_x1 = 0;
                        box3_x2 = box3_image.Width;
                        box3_y1 = 0;
                        box3_y2 = box3_image.Height;
                    }
                    Mat box3_zoom = new Mat(box3_image, new Rect(box3_x1, box3_y1, box3_x2 - box3_x1, box3_y2 - box3_y1));
                    pictureBox3.Image = MatToBitmap(box3_zoom);
                }
                else if (box3_mode == 3)
                {
                    double x_zoom = (double)pictureBox1.Width / (double)box3_image.Width;
                    double y_zoom = (double)pictureBox1.Height / (double)box3_image.Height;
                    double zoom;
                    double x_pos, y_pos;
                    if (x_zoom < y_zoom)
                    {
                        zoom = x_zoom;
                        x_pos = e.X / zoom;
                        y_pos = e.Y / zoom - (((double)pictureBox1.Height - (double)box3_image.Height * zoom) / 2) / zoom;
                    }
                    else
                    {
                        zoom = y_zoom;
                        y_pos = e.Y / zoom;
                        x_pos = e.X / zoom - ((pictureBox1.Width - (double)box3_image.Width * zoom) / 2) / zoom;
                    }
                    double org_w = box3_x2 - box3_x1, org_h = box3_y2 - box3_y1
                        , aft_w = box3_image.Width, aft_h = box3_image.Height;
                    x_pos = x_pos - (aft_w / 2);// * box3_zoom_value - ((double)box3_image.Width* box3_zoom_value - (double)(box3_x2 - box3_x1)) / 2/ box3_zoom_value;
                    y_pos = y_pos - (aft_h / 2);// *box3_zoom_value-((double)box3_image.Height* box3_zoom_value - (double)(box3_y2 - box3_y1)) / 2/ box3_zoom_value;
                    int count = 0;
                    x_pos /= (box3_zoom_value);
                    y_pos /= (box3_zoom_value);
                    System.Console.WriteLine(e.X.ToString() + "," + e.Y.ToString());
                    foreach (Cirlcle circle1 in box3_circles)
                    {
                        Scalar color = Scalar.White;
                        int thick = 1;
                        if (x_pos > (double)(circle1.line1_x1- aft_w / 2) && x_pos < (double)(circle1.line1_x2 - aft_w / 2))
                        {
                            if (y_pos > (double)(circle1.line1_y - aft_h/ 2-10)  && y_pos < (double)(circle1.line1_y - aft_h / 2 + 10) )
                            {
                                color = Scalar.Red;
                                count++;
                            }
                        }
                        if (x_pos > (double)(circle1.line2_x - aft_w / 2 - 10) && x_pos < (double)(circle1.line2_x - aft_w / 2 + 10) )
                        {
                            if (y_pos > (double)(circle1.line2_y1 - aft_h / 2)  && y_pos < (double)(circle1.line2_y2 - aft_h / 2) )
                            {
                                color = Scalar.Red;
                                count++;
                            }
                        }
                        Cv2.PutText(box3_image, circle1.text, circle1.text_pos, HersheyFonts.HersheyPlain, 2, color);
                        Cv2.Circle(box3_image, (int)(circle1.centerX), (int)(circle1.centerY), (int)circle1.radius, color, thick, LineTypes.Link8, 0);
                        box3_image.Line(circle1.line1_x1, circle1.line1_y, circle1.line1_x2, circle1.line1_y, color);
                        box3_image.Line(circle1.line2_x, circle1.line2_y1, circle1.line2_x, circle1.line2_y2, color);
                        box3_image = DrawArrow(box3_image, new OpenCvSharp.Point(circle1.centerX, circle1.centerY), new OpenCvSharp.Point(circle1.arrow_x2, circle1.arrow_y2), 15, 30, color);
                    }
                    if (count == 0)
                    {
                        foreach (Cirlcle circle1 in box3_circles)
                        {
                            Scalar color = Scalar.White;
                            int thick = 1;
                            Cv2.PutText(box3_image, circle1.text, circle1.text_pos, HersheyFonts.HersheyPlain, 2, color);
                            Cv2.Circle(box3_image, (int)(circle1.centerX), (int)(circle1.centerY), (int)circle1.radius, color, thick, LineTypes.Link8, 0);
                            box3_image.Line(circle1.line1_x1, circle1.line1_y, circle1.line1_x2, circle1.line1_y, color);
                            box3_image.Line(circle1.line2_x, circle1.line2_y1, circle1.line2_x, circle1.line2_y2, color);
                            box3_image = DrawArrow(box3_image, new OpenCvSharp.Point(circle1.centerX, circle1.centerY), new OpenCvSharp.Point(circle1.arrow_x2, circle1.arrow_y2), 15, 30, color);
                        }
                    }
                    Mat box3_zoom = new Mat(box3_image, new Rect(box3_x1, box3_y1, box3_x2 - box3_x1, box3_y2 - box3_y1));
                    pictureBox3.Image = MatToBitmap(box3_zoom);
                }
            }
            
        }

        private void pictureBox3_MouseUp(object sender, MouseEventArgs e)
        {
            box3_mousedown = false;
            box3_mouseup = true;
        }

        private void pictureBox3_MouseMove(object sender, MouseEventArgs e)
        {

        }

        private Mat DrawArrow(Mat img, OpenCvSharp.Point pStart, OpenCvSharp.Point pEnd, int len, int alpha, OpenCvSharp.Scalar color)
        {
            const double PI = Math.PI;
            OpenCvSharp.Point arrow=new OpenCvSharp.Point();
            //计算 θ 角（最简单的一种情况在下面图示中已经展示，关键在于 atan2 函数，详情见下面）   
            double angle = Math.Atan2((double)(pStart.Y - pEnd.Y), (double)(pStart.X - pEnd.X));
            Cv2.Line(img, pStart.X,pStart.Y, pEnd.X, pEnd.Y, color, 2);
            //计算箭角边的另一端的端点位置（上面的还是下面的要看箭头的指向，也就是pStart和pEnd的位置） 
            arrow.X = pEnd.X + (int)(len * Math.Cos(angle + PI * alpha / 180));
            arrow.Y = pEnd.Y + (int)(len * Math.Sin(angle + PI * alpha / 180));
            Cv2.Line(img, pEnd.X, pEnd.Y, arrow.X, arrow.Y, color, 2);
            arrow.X = pEnd.X + (int)(len * Math.Cos(angle - PI * alpha / 180));
            arrow.Y = pEnd.Y + (int)(len * Math.Sin(angle - PI * alpha / 180));
            Cv2.Line(img, pEnd.X, pEnd.Y, arrow.X, arrow.Y, color, 2);
            return img;
        }
        private Save_var detect_circle(Mat img_copy1, 
            List<Cirlcle> cirlcles,
            int count,
            RotatedRect rrt,
            double r2,
            double distanse,
            double focus)
        {
            Save_var save_var=new Save_var();
            Boolean f1 = true, f2 = true;
            int count_match = -1;
            for (int j = 0; j < cirlcles.Count; j++)
            {
                Cirlcle cirlcle = cirlcles[j];
                Boolean match1 = cirlcle.centerX / rrt.Center.X < 1.005 && cirlcle.centerX / rrt.Center.X > 0.995;
                Boolean match2 = cirlcle.centerY / rrt.Center.Y < 1.005 && cirlcle.centerY / rrt.Center.Y > 0.995;
                if (match1 && match2)
                {
                    f1 = false;
                }
                else
                {
                    count_match++;
                }
                if (cirlcle.radius / r2 < 1.005 && cirlcle.radius / r2 > 0.995)
                {
                    f2 = false;
                }
            };
            if (f1 && f2)
            {
                //Cv2.Circle(img_copy1, (int)(rrt.Center.X), (int)(rrt.Center.Y), (int)r2, new Scalar(255, 255, 255), 1, LineTypes.Link8, 0);
                /*
                img_copy1.Line((int)(rrt.Center.X)-img_copy1.Width/40, (int)(rrt.Center.Y), (int)(rrt.Center.X) + img_copy1.Width / 40, (int)(rrt.Center.Y), Scalar.White);
                img_copy1.Line((int)(rrt.Center.X), (int)(rrt.Center.Y) - img_copy1.Width / 40, (int)(rrt.Center.X),(int)(rrt.Center.Y) + img_copy1.Width / 40, Scalar.White);
                */
                double end_x = rrt.Center.X + r2 * Math.Cos(30 * count_match * 1.0 / 180 * Math.PI);
                double end_y = rrt.Center.Y + r2 * Math.Sin(30 * count_match * 1.0 / 180 * Math.PI);
                //img_copy1 = DrawArrow(img_copy1, new OpenCvSharp.Point(rrt.Center.X, rrt.Center.Y), new OpenCvSharp.Point(end_x, end_y), 15, 30, new Scalar(255, 255, 255));
                OpenCvSharp.Point text_po;
                double move = 20;
                if (move> r2 / 2)
                {
                    move = r2 / 2;
                }
                text_po.X = (int)((end_x + rrt.Center.X - move) / 2);
                text_po.Y = (int)((end_y + rrt.Center.Y - move) / 2);
                //Cv2.PutText(img_copy1, (distanse / focus * r2).ToString("#0.0")+"(e="+(rrt.Size.Width/ rrt.Size.Height).ToString("#0.00")+")", text_po, HersheyFonts.HersheyPlain, 2, Scalar.All(255));
                Cirlcle cirlcle = new Cirlcle();
                cirlcle.centerX = rrt.Center.X;
                cirlcle.centerY = rrt.Center.Y;
                cirlcle.radius = r2;
                cirlcle.arrow_x2 = end_x;
                cirlcle.arrow_y2 = end_y;
                cirlcle.text_pos = text_po;
                cirlcle.text = (distanse / focus * r2).ToString("#0.0") + "(e=" + (rrt.Size.Width / rrt.Size.Height).ToString("#0.00") + ")";
                cirlcle.line1_x1 = (int)(rrt.Center.X) - img_copy1.Width / 40;
                cirlcle.line1_x2 = (int)(rrt.Center.X) + img_copy1.Width / 40;
                cirlcle.line1_y = (int)(rrt.Center.Y);
                cirlcle.line2_y1 = (int)(rrt.Center.Y) - img_copy1.Width / 40;
                cirlcle.line2_y2 = (int)(rrt.Center.Y) + img_copy1.Width / 40;
                cirlcle.line2_x = (int)(rrt.Center.X);
                cirlcles.Add(cirlcle);
                ellipse_save.Add(rrt);
                count++;
            }
            save_var.img_copy1 = img_copy1;
            save_var.count = count;
            save_var.cirlcles = cirlcles;
            return save_var;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (box3_lock != true)
            {
                box3_mode = 1;
                pictureBox3.Cursor = Cursors.NoMove2D;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (box3_lock != true)
            {
                box3_mode = 2;
                pictureBox3.Cursor = Cursors.SizeAll;
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (box3_lock != true)
            {
                box3_mode = 3;
                pictureBox3.Cursor = Cursors.Cross;
            }
        }

        private detectSave detect_line_to_draw(List<LineSave> lineSave,Mat pre_image, List<LineDrawSave> lineDrawSave)
        {
            for (int i = 0; i < lineSave.Count; i++)
            {
                LineSave lineSave2 = lineSave[i];
                List<LineSegmentPoint> line_parts = lineSave2.lines;
                int x_max = (int)line_parts[0].P1.X,
                        x_min = (int)line_parts[0].P1.X,
                        y_max = (int)line_parts[0].P1.Y,
                        y_min = (int)line_parts[0].P1.Y;
                for (int j = 0; j < line_parts.Count; j++)
                {
                    LineSegmentPoint line_part = line_parts[j];
                    int x1 = line_part.P1.X,
                        x2 = line_part.P2.X,
                        y1 = line_part.P1.Y,
                        y2 = line_part.P2.Y;
                    if (x1 > x_max)
                    {
                        x_max = x1;
                    }
                    if (x2 > x_max)
                    {
                        x_max = x2;
                    }
                    if (x1 < x_min)
                    {
                        x_min = x1;
                    }
                    if (x2 < x_min)
                    {
                        x_min = x2;
                    }

                    if (y1 > y_max)
                    {
                        y_max = y1;
                    }
                    if (y2 > y_max)
                    {
                        y_max = y2;
                    }
                    if (y1 < y_min)
                    {
                        y_min = y1;
                    }
                    if (y2 < y_min)
                    {
                        y_min = y2;
                    }
                    //pre_cp.Line(line_part.P1, line_part.P2, Scalar.White, 3, LineTypes.AntiAlias, 0);
                }
                Point point1, point2;
                if (x_max == x_min || y_max == y_min)
                {
                    point1 = new Point(x_min, y_min);
                    point2 = new Point(x_max, y_max);
                    pre_image.Line(point1, point2, Scalar.Black, 2, LineTypes.AntiAlias, 0);
                }
                else
                {
                    int x_w_bd = x_max - x_min + 2, y_h_bd = y_max - y_min + 2;
                    if (y_min == 0)
                    {
                        y_min = 1;
                    }
                    if (x_min == 0)
                    {
                        x_min = 1;
                    }
                    Mat mask_image = new Mat(pre_image, new Rect(x_min - 1, y_min - 1, x_w_bd, y_h_bd));
                    var mask_indexer = mask_image.GetGenericIndexer<Vec3b>();
                    List<Point> mask_points = new List<Point>();
                    double point_have = 0;
                    for (int row = 0; row < mask_image.Rows; row++)
                    {
                        for (int col = 0; col < mask_image.Cols; col++)
                        {
                            if (mask_indexer[row, col].Item0 != 0 || mask_indexer[row, col].Item1 != 0 || mask_indexer[row, col].Item2 != 0)
                            {
                                mask_points.Add(new Point(col, row));
                                point_have++;
                            }
                        }
                    }
                    if (point_have < (x_max - x_min+ y_max - y_min) / 10) continue;
                    if (mask_points.Count == 0) continue;
                    SaveConvertLinePoints point12 =
                                    Convert_points_to_line_points(mask_points.ToArray(),
                                        0, mask_image.Cols,
                                        0, mask_image.Rows, x_min - 1, y_min - 1);
                    point1 = point12.point1;
                    point2 = point12.point2;
                    pre_image.Rectangle(new Rect(x_min - 1, y_min - 1, x_max - x_min + 2, y_max - y_min + 2), Scalar.Black, -1);
                }
                LineDrawSave ls1 = new LineDrawSave();
                ls1.p1 = point1;
                ls1.p2 = point2;
                Point point3 = point1 - point2;
                ls1.angle = Math.Atan2(point3.Y, point3.X) / Math.PI * 180;
                ls1.dis= Math.Sqrt(point3.X * point3.X + point3.Y * point3.Y);
                lineDrawSave.Add(ls1);
            }
            detectSave detect_save=new detectSave();
            detect_save.pre_image = pre_image;
            detect_save.lineDrawSave = lineDrawSave;
            return detect_save;
        }
        private List<LineSave> detect_line(List<LineSave> lineSave,
            Mat child_rect,double thresh,
            List<Point> line_points,
            Rect rectangle, RotatedRect rect_rot,Boolean pass_all_degree)
        {
            LineSegmentPoint[] lines_ch =
                Cv2.HoughLinesP(child_rect, 1,
                    Math.PI / 180,
                        (int)thresh);
            int degree_precise = 5;
            if (thresh == 0)
            {
                degree_precise = 2;
            }
            double rot_angle = Math.Abs(rect_rot.Angle / 180 * Math.PI);
            foreach (LineSegmentPoint segStd in lines_ch)
            {
                Point p1 = segStd.P1 + new Point(rectangle.X, rectangle.Y);
                Point p2 = segStd.P2 + new Point(rectangle.X, rectangle.Y);
                Point p3 = p1 - p2;
                LineSegmentPoint s1 = new LineSegmentPoint(p1, p2);
                int x_diff = p3.X, y_diff = p3.Y;
                if (y_diff < 0)
                {
                    x_diff = -x_diff;
                    y_diff = -y_diff;
                }
                double diff = x_diff * x_diff + y_diff * y_diff;
                double distance = Math.Sqrt((double)diff);
                double theta = Math.Atan2(y_diff, x_diff);
                if (distance < thresh)
                {
                    continue;
                }
                line_points.Add(p1);
                line_points.Add(p2);
                double diff_angle1 = Math.Abs(theta - (Math.PI / 2 + rot_angle));
                double diff_angle2 = Math.Abs(theta - (rot_angle));
                if (Math.Abs(diff_angle1 - Math.PI) < 0.01)
                {
                    diff_angle1 = 0;
                }
                else
                {
                    diff_angle1 = Math.Abs(theta - Math.PI / 2);
                    if (Math.Abs(diff_angle1 - Math.PI) < 0.01)
                    {
                        diff_angle1 = 0;
                    }
                }
                if (Math.Abs(diff_angle2 - Math.PI) < 0.01)
                {
                    diff_angle2 = 0;
                }
                else
                {
                    diff_angle2 = Math.Abs(theta);
                    if (Math.Abs(diff_angle2 - Math.PI) < 0.01)
                    {
                        diff_angle2 = 0;
                    }
                }
                if ((diff_angle1 < 0.01) || (diff_angle2 < 0.01) || pass_all_degree)
                {
                    Boolean add_flag = true;
                    double theta_s = Math.Round(theta / Math.PI * 180 / degree_precise) * degree_precise;
                    if (theta_s == 180)
                    {
                        theta_s = 0;
                    }
                    List<LineSave> line_select = lineSave.Where(gp => gp.angle == theta_s).ToList();
                    if (degree_precise == 2)
                    {
                    }
                    else
                    {
                        for (int j = 0; j < line_select.Count; j++)
                        {
                            double x_to_pos = (p1 + p2).X / 2 - line_select[j].posx;
                            double y_to_pos = (p1 + p2).Y / 2 - line_select[j].posy;
                            if (y_to_pos < 0)
                            {
                                y_to_pos = -y_to_pos;
                                x_to_pos = -x_to_pos;
                            }
                            double angle_to_pos = Math.Round(Math.Atan2(y_to_pos, x_to_pos) / Math.PI * 180 / degree_precise) * degree_precise;
                            if (angle_to_pos == 180)
                            {
                                angle_to_pos = 0;
                            }
                            if (Math.Abs(angle_to_pos - line_select[j].angle) <= degree_precise)
                            {
                                line_select[j].lines.Add(s1);
                                add_flag = false;
                                break;
                            }
                        }
                    }
                    
                    if (add_flag)
                    {
                        LineSave lineSave1 = new LineSave();
                        lineSave1.angle = theta_s;
                        lineSave1.posx = (p1.X + p2.X) / 2;
                        lineSave1.posy = (p1.Y + p2.Y) / 2;
                        lineSave1.lines.Add(s1);
                        lineSave.Add(lineSave1);
                    }
                    //pre_cp.Line(p1, p2, Scalar.White, 3, LineTypes.AntiAlias, 0);
                }

            }
            return lineSave;
        }
    }
}